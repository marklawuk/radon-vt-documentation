
Introduction
************

Welcome to the documentation page for the Architecture Verification Tool. The
primary purpose of the verification tool is to allow a user to verify that a
given FaaS architecture complies with a set of functional and non-functional
requirements. These requirements are encoded using a new language, based on
logic, called the *constraint definition language* (CDL).

In its primary mode, the tool takes as input a FaaS architecture and a CDL
specification, and outputs a list of issues with the FaaS architecture (i.e. a
list of violations of the CDL specification). The user can then use this
information to correct their FaaS architecture. As correction of a FaaS
architecture, and specification of requirements can be a hard task for
non-expert users, the tool also provides two additional modes in order to help
a user correct a non-compliant architecture, and to aid the user to specify
requirements in the CDL.
