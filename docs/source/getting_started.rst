Getting Started with the Verification Tool plugin
**********************************

The verification tool is available as a plugin for Eclipse Che. To request a
license to use the plugin, please `fill in this form
<https://imperial.eu.qualtrics.com/jfe/form/SV_1HthWM3xILQM6ih>`_.

Upon obtaining a license, the following lines should be added to the Devfile of
your Eclipse Che workspace. Doing so will trigger the installation of the
verification tool plugin.

.. code-block:: yaml
   :linenos:

   components:
     - type: chePlugin
       reference: >-
         https://raw.githubusercontent.com/radon-h2020/radon-plugin-registry/master/radon/radon-vt/latest/meta.yaml
     - mountSources: true
       endpoints:
         - name: radon-vt
           port: 5000
           attributes:
             protocol: http
             public: 'true'
             discoverable: 'false'
             secure: 'false'
       referenceContent: |
         ---
         apiVersion: apps/v1
         kind: Deployment
         metadata:
           name: vt-deployment
           labels:
             app: vt
             tier: frontend
         spec:
           replicas: 1
           selector:
             matchLabels:
               app: vt
               tier: frontend
           template:
             metadata:
               labels:
                 app: vt
                 tier: frontend
             spec:
               containers:
                 - name: vt
                   image: marklawimperial/verification-tool:latest
                   imagePullPolicy: Always
                   ports:
                     - containerPort: 5000
       type: kubernetes
       alias: radon-vt


To get started, you can clone the `verification tool sample project
<https://github.com/radon-h2020/demo-verification-tool-sample-project>`_. This
contains a sample TOSCA model and a CDL specification.

.. figure:: clone.gif

The file main.cdl contains the following CDL specification:

.. literalinclude:: main.cdl

In this scenario, the architecture consists of several buckets, located in
various countries, and a lambda function "create thumbnails", which stores an
image in one of the buckets. The CDL specification expresses that the
"create_thumbnails" function must meet certain requirements (given by its
pre/post/invariant conditions). These are that, provided that the country of
origin is supported, the image should be stored in at least one bucket, and
that that bucked must be located in a country with whom the country of origin
is willing to share. This implies that for each supported country, the
architecture must contain at least one bucket located in a country with whom
the original country is willing to share.

Right-clicking on the file "main.cdl" will allow the user to run the
verification tool (by clicking the "Verify" option). The tool will then search
for inconsistencies between the specification and the architecture, and will
return any it finds.

.. figure:: verify.gif

In this case, two inconsistencies are found, which correspond to the two
supported countries for whom there is no bucket in which they would be willing
to store their data (China and the USA).

Alternatively, it is also possible to start from a blank specification and try
your own example. For more information on what it is possible to write in the
CDL, please see `this document
<http://radon-h2020.eu/wp-content/uploads/2020/01/D4.1-Constraint-definition-language-I.pdf>`_.
All files containing CDL specifications should have the extension ".cdl".



