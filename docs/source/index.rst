.. Verification Tool documentation master file, created by
   sphinx-quickstart on Tue May 19 10:07:37 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the Architecture Verification Tool's documentation!
===================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :numbered:

.. include:: business_use.rst
.. include:: technical_overview.rst
.. include:: getting_started.rst







.. Indices and tables
.. ==================
.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
