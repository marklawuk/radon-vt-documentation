
Technical Overview
******************

The Constraint Definition Language (CDL) provides a way of expressing the high
level functional and non-functional requirements of a FaaS architecture. The
final version of the associated Verification Tool (VT) will have three main
modes of execution, detailed below.

1. Verification. In this mode, the tool checks whether a given FaaS
   architecture (expressed as a TOSCA model) complies with the constraints
   expressed in a given CDL specification.
2. Correction. In this mode, the tool searches for a modification of a
   (non-compliant) TOSCA model, in order to make it comply with the constraints
   in a given CDL specification.
3. Learning. In this mode, the tool is given examples of valid and invalid
   TOSCA models, or execution traces, and a partial CDL specification. The tool
   then searches for an extension of the CDL specification which rules out all
   of the invalid examples, which maintaining consistency with the valid
   examples.

.. raw:: html

    </br>
    <iframe width="560" height="315" src="https://www.youtube.com/embed/pJWetOzY2Zc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </br>
    </br>
    <caption>A demonstration of the verification and correction modes using a commandline version of the Verification Tool.</caption>
    </br>
    </br>
    </br>

